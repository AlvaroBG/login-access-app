import { LitElement, html, css } from 'lit-element';
import { openWcLogo } from './open-wc-logo.js';

export class LoginAccessApp extends LitElement {
  static get properties() {
    return {
      title: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        background: #fff;
        padding: 0px;
        margin: 0px;
        font-family: 'Nunito', sans-serif;
        font-size: 16px;
      }

      main {
        flex-grow: 1;
      }

      .logo > svg {
        margin-top: 36px;
        animation: app-logo-spin infinite 20s linear;
      }

      @keyframes app-logo-spin {
        from {
          transform: rotate(0deg);
        }
        to {
          transform: rotate(360deg);
        }
      }

      .app-footer {
        font-size: calc(12px + 0.5vmin);
        align-items: center;
      }

      .app-footer a {
        margin-left: 5px;
      }
    `;
  }

  constructor() {
    super();
    this.title = 'My app';
  }

  render() {
    return html`
      <main>
        <div id="login_div" class="main-div">
          <h3>Firebase Web login Example</h3>
          <input type="email" placeholder="Email..." id="email_field" />
          <input type="password" placeholder="Password..." id="password_field" />

          <button onclick="login()">Login to Account</button>
        </div>

        <div id="user_div" class="loggedin-div">
          <h3>Welcome User</h3>
          <p id="user_para">Welcome to Firebase web login Example. You're currently logged in.</p>
          <button onclick="logout()">Logout</button>
        </div>
      </main>
    `;
  }
}
