import { html } from 'lit-html';
import '../src/login-access-app.js';

export default {
  title: 'LoginAccessApp',
  component: 'login-access-app',
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

function Template({ title, backgroundColor }) {
  return html`
    <login-access-app
      style="--login-access-app-background-color: ${backgroundColor || 'white'}"
      .title=${title}
    >
    </login-access-app>
  `;
}

export const App = Template.bind({});
App.args = {
  title: 'My app',
};
